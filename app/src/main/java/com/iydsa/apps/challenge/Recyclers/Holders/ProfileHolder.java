package com.iydsa.apps.challenge.Recyclers.Holders;

import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.R;

public class ProfileHolder extends RecyclerView.ViewHolder {
    public ImageView Imagen;

    public ProfileHolder(View itemView) {
        super(itemView);
        Imagen = itemView.findViewById(R.id.foto_home);
    }
}
