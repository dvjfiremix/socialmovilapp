package com.iydsa.apps.challenge.Recyclers.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.R;
import com.iydsa.apps.challenge.Recyclers.Holders.HomeImageHolder;

import java.util.ArrayList;
import java.util.Random;

import Libraryes.Utils;

public class HomeImageAdapter extends RecyclerView.Adapter<HomeImageHolder> {

    private Context context;
    private ArrayList<Integer> home;
    private Utils utils;

    public HomeImageAdapter(Context ctx, ArrayList<Integer> stories) {
        this.context = ctx;
        this.home = stories;
        utils = Utils.getInstance(ctx);
    }

    @NonNull
    @Override
    public HomeImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_story, null);
        return new HomeImageHolder(v);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull HomeImageHolder holder, int position) {
        utils.LoadImage(home.get(position), holder.Homeimage);
        Random n = new Random();
        int n1 = n.nextInt(999);
        holder.mensajes.setText("" + n1);
        Random r = new Random();
        int n2 = r.nextInt(999);
        holder.likes.setText("" + n2);
        holder.Homeimage.setOnClickListener(v -> utils.CustomMesaje(" Este es un mensaje de la imágen " + position, home.get(position)));
    }

    @Override
    public int getItemCount() {
        return home.size();
    }

}
