package com.iydsa.apps.challenge.Recyclers.Holders;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class StoryHolder extends RecyclerView.ViewHolder {
    public CircleImageView foto;
    public CircleImageView otra;
    public RelativeLayout mifoto;
    public RelativeLayout other;

    public StoryHolder(View itemView) {
        super(itemView);
        foto = itemView.findViewById(R.id.foto);
        otra = itemView.findViewById(R.id.otra);
        mifoto = itemView.findViewById(R.id.myphoto);
        other = itemView.findViewById(R.id.other);
    }
}
