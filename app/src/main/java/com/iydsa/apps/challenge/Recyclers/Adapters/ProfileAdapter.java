package com.iydsa.apps.challenge.Recyclers.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.R;
import com.iydsa.apps.challenge.Recyclers.Clases.ProfileClass;
import com.iydsa.apps.challenge.Recyclers.Holders.ProfileHolder;

import java.util.ArrayList;

import Libraryes.Utils;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileHolder> {

    private Context context;
    private ArrayList<ProfileClass> home;
    private Utils utils;

    public ProfileAdapter(Context ctx, ArrayList<ProfileClass> inicio) {
        this.context = ctx;
        this.home = inicio;
        utils = Utils.getInstance(ctx);
    }

    @NonNull
    @Override
    public ProfileHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_profile, null);
        return new ProfileHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileHolder holder, int position) {
        final ProfileClass datos = home.get(position);
        if (datos.getFoto() == 0) {
            utils.LoadImageUrl(datos.getImageFoto(), holder.Imagen);
        } else {
            utils.LoadImage(datos.getFoto(), holder.Imagen);
        }

        holder.Imagen.setOnClickListener(v -> {
            if (datos.getFoto() == 0) {
                utils.CustomMesaje("Imágen de la galería de perfil desde internet " + position, datos.getImageFoto());
            } else {
                utils.CustomMesaje("Imágen de la galería de perfil " + position, datos.getFoto());
            }

        });

    }

    @Override
    public int getItemCount() {
        return home.size();
    }
}
