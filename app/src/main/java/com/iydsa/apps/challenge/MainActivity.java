package com.iydsa.apps.challenge;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.Recyclers.Adapters.Home_adapter;
import com.iydsa.apps.challenge.Recyclers.Adapters.StorysAdapter;
import com.iydsa.apps.challenge.Recyclers.Clases.HomeClass;
import com.iydsa.apps.challenge.Recyclers.Clases.storyclass;

import java.util.ArrayList;

import Libraryes.Utils;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    RecyclerView storyes;
    RecyclerView homes;
    StorysAdapter storysAdapter;
    Home_adapter home_adapter;
    ArrayList<storyclass> historias = new ArrayList<>();
    ArrayList<HomeClass> inicio = new ArrayList<>();
    storyclass datos;
    HomeClass inicioclass;
    CircleImageView foto;
    private Utils utils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        utils = Utils.getInstance(this);
        foto = findViewById(R.id.foto);
        utils.LoadImageUrl("https://graph.facebook.com/1466428176/picture?height=500", foto);
        storyes = findViewById(R.id.historias);
        homes = findViewById(R.id.homes);
        storysAdapter = new StorysAdapter(this, historias);
        home_adapter = new Home_adapter(this, inicio);
        storyes.setAdapter(storysAdapter);
        homes.setAdapter(home_adapter);
        foto.setOnClickListener(v -> {
            Intent n = new Intent(getApplicationContext(), Profile.class);
            startActivity(n);
            finish();
        });
        LoadStories();
        LoadHome();
    }

    private void LoadStories() {
        datos = new storyclass();
        datos.setFoto(R.drawable.dev);
        datos.setFoto2(R.drawable.dev);
        datos.setImageUrl("https://graph.facebook.com/1466428176/picture?height=500");
        datos.setUser("Christian Arreola");
        datos.setPlace("Monterrey, Nuevo León");
        datos.setCuent_comment(800);
        datos.setCuenta_like(300);
        datos.setTexto("Esta es mi primera foto para la actividad de Story Full de Octubre del 2020");
        historias.add(datos);

        datos = new storyclass();
        datos.setFoto(R.drawable.dev);
        datos.setFoto2(R.drawable.dev);
        datos.setImageUrl("https://graph.facebook.com/1466428176/picture?height=500");
        datos.setUser("Christian Arreola");
        datos.setPlace("Monterrey, Nuevo León");
        datos.setCuent_comment(800);
        datos.setCuenta_like(300);
        datos.setTexto("Programador y Diseñador de aplicaciones móviles para S.O. Android con estilo Material Design");
        historias.add(datos);

        datos = new storyclass();
        datos.setFoto(R.drawable.carlos);
        datos.setFoto2(R.drawable.dev);
        datos.setUser("Carlos Chirinos");
        datos.setPlace("Ica, Perú");
        datos.setCuent_comment(500);
        datos.setCuenta_like(100);
        datos.setTexto("Diseñador de interfaces de aplicaciones, software y cualquier producto, este es un ejemplo del mismo");
        historias.add(datos);

        datos = new storyclass();
        datos.setFoto(R.drawable.image_17);
        datos.setFoto2(R.drawable.dev);
        datos.setUser("Usuario demo");
        datos.setPlace("Escuintla, Chiapas");
        datos.setCuent_comment(60);
        datos.setCuenta_like(800);
        datos.setTexto("Este es un texto de ejemplo para probar que el sistema está funcionando correctamente, demo usuario");
        historias.add(datos);

        datos = new storyclass();
        datos.setFoto(R.drawable.image_16);
        datos.setFoto2(R.drawable.dev);
        datos.setUser("Usuario demo 2");
        datos.setPlace("Tapachula, Chiapas");
        datos.setCuent_comment(735);
        datos.setCuenta_like(200);
        datos.setTexto("Este es un texto de ejemplo para probar que el sistema está funcionando correctamente, demo usuario 2");
        historias.add(datos);

        datos = new storyclass();
        datos.setFoto(R.drawable.image_15);
        datos.setFoto2(R.drawable.dev);
        datos.setUser("Usuario demo 3");
        datos.setPlace("Tuxtla Gutierrez, Chiapas");
        datos.setCuent_comment(65);
        datos.setCuenta_like(250);
        datos.setTexto("Este es un texto de ejemplo para probar que el sistema está funcionando correctamente, demo usuario 3");
        historias.add(datos);

        datos = new storyclass();
        datos.setFoto(R.drawable.profile);
        datos.setFoto2(R.drawable.dev);
        datos.setUser("Usuario demo 4");
        datos.setPlace("Acapetahua, Chiapas");
        datos.setCuent_comment(150);
        datos.setCuenta_like(280);
        datos.setTexto("Este es un texto de ejemplo para probar que el sistema está funcionando correctamente, demo usuario 4");
        historias.add(datos);

        storysAdapter.notifyDataSetChanged();
    }

    private void LoadHome() {
        inicioclass = new HomeClass();
        inicioclass.setImage(R.drawable.dev);
        inicioclass.setImageUrl("https://graph.facebook.com/1466428176/picture?height=500");
        inicioclass.setUser("Christian Arreola");
        inicioclass.setPlace("Monterrey, Nuevo León");
        ArrayList<Integer> imagenes = new ArrayList<>();
        imagenes.add(R.drawable.dev);
        imagenes.add(R.drawable.dev0);
        imagenes.add(R.drawable.dev1);
        imagenes.add(R.drawable.dev2);
        imagenes.add(R.drawable.dev3);
        imagenes.add(R.drawable.dev4);
        inicioclass.setImagenes(imagenes);
        inicio.add(inicioclass);

        inicioclass = new HomeClass();
        inicioclass.setImage(R.drawable.carlos);
        inicioclass.setUser("Carlos Chirinos");
        inicioclass.setPlace("Ica, Perú");
        ArrayList<Integer> imagenes2 = new ArrayList<>();
        imagenes2.add(R.drawable.carlos);
        imagenes2.add(R.drawable.carlos2);
        imagenes2.add(R.drawable.carlos3);
        imagenes2.add(R.drawable.carlos4);
        imagenes2.add(R.drawable.carlos5);
        imagenes2.add(R.drawable.carlos6);
        inicioclass.setImagenes(imagenes2);
        inicio.add(inicioclass);

        inicioclass = new HomeClass();
        inicioclass.setImage(R.drawable.image_14);
        inicioclass.setUser("Usuario Demo");
        inicioclass.setPlace("Escuintla, Chiapas");
        ArrayList<Integer> imagenes3 = new ArrayList<>();
        imagenes3.add(R.drawable.image_15);
        imagenes3.add(R.drawable.image_16);
        imagenes3.add(R.drawable.image_17);
        imagenes3.add(R.drawable.image_14);
        inicioclass.setImagenes(imagenes3);
        inicio.add(inicioclass);

        inicioclass = new HomeClass();
        inicioclass.setImage(R.drawable.image_17);
        inicioclass.setUser("Usuario Demo 2");
        inicioclass.setPlace("Tuxtla Gutierrez, Chiapas");
        ArrayList<Integer> imagenes4 = new ArrayList<>();
        imagenes4.add(R.drawable.image_17);
        imagenes4.add(R.drawable.image_16);
        imagenes4.add(R.drawable.image_10);
        imagenes4.add(R.drawable.image_14);
        inicioclass.setImagenes(imagenes4);
        inicio.add(inicioclass);

        inicioclass = new HomeClass();
        inicioclass.setImage(R.drawable.image_16);
        inicioclass.setUser("Usuario Demo 3");
        inicioclass.setPlace("Tapachula, Chiapas");
        ArrayList<Integer> imagenes5 = new ArrayList<>();
        imagenes5.add(R.drawable.image_14);
        imagenes5.add(R.drawable.image_16);
        imagenes5.add(R.drawable.image_15);
        imagenes5.add(R.drawable.image_17);
        inicioclass.setImagenes(imagenes5);
        inicio.add(inicioclass);

        home_adapter.notifyDataSetChanged();
    }
}