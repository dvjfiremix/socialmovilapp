package com.iydsa.apps.challenge.Recyclers.Clases;

public class storyclass {
    private int foto;
    private int foto2;
    private String user;
    private String place;
    private String texto;
    private int cuenta_like;
    private int cuent_comment;
    private String imageUrl;

    public storyclass() {

    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getCuenta_like() {
        return cuenta_like;
    }

    public void setCuenta_like(int cuenta_like) {
        this.cuenta_like = cuenta_like;
    }

    public int getCuent_comment() {
        return cuent_comment;
    }

    public void setCuent_comment(int cuent_comment) {
        this.cuent_comment = cuent_comment;
    }

    public int getFoto2() {
        return foto2;
    }

    public void setFoto2(int foto2) {
        this.foto2 = foto2;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
