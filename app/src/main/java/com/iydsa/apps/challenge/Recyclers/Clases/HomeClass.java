package com.iydsa.apps.challenge.Recyclers.Clases;

import java.util.ArrayList;

public class HomeClass {
    private String User;
    private String place;
    private ArrayList<Integer> imagenes;
    private int Image;
    private String imageUrl;

    public HomeClass() {

    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }

    public ArrayList<Integer> getImagenes() {
        return imagenes;
    }

    public void setImagenes(ArrayList<Integer> imagenes) {
        this.imagenes = imagenes;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
