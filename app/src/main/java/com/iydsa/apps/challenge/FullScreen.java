package com.iydsa.apps.challenge;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

import Libraryes.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.bullyboo.text_animation.AnimationBuilder;
import ru.bullyboo.text_animation.TextCounter;

public class FullScreen extends AppCompatActivity {

    CircleImageView foto, foto2;
    ImageView Fullmage;
    TextView usuario, lugar, info, comentarios, likes;
    int fot, fot2, comentario, like;
    String users, places, mensajes;
    LinearLayout cerrar;
    private Utils utils;

    public FullScreen() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_full);
        utils = Utils.getInstance(this);
        foto = findViewById(R.id.foto);
        foto2 = findViewById(R.id.imagen);
        Fullmage = findViewById(R.id.background);
        usuario = findViewById(R.id.user);
        lugar = findViewById(R.id.place);
        info = findViewById(R.id.info_text);
        comentarios = findViewById(R.id.comment);
        likes = findViewById(R.id.likes);
        cerrar = findViewById(R.id.close);

        cerrar.setOnClickListener(v -> finish());

        CancelAutomatically(10000);

        Intent n = getIntent();
        if (n != null) {
            fot = n.getIntExtra("FOTO", R.drawable.dev);
            fot2 = n.getIntExtra("FOTO2", R.drawable.dev);
            users = n.getStringExtra("USUARIO");
            places = n.getStringExtra("PLACE");
            mensajes = n.getStringExtra("INFO");
            comentario = n.getIntExtra("COMENTARIOS", 0);
            like = n.getIntExtra("LIKES", 0);

            Random r = new Random();
            int cuenta = r.nextInt(999);

            Random r2 = new Random();
            int cuenta2 = r2.nextInt(999);
            if (users.contains("Christian Arreola")) {
                utils.LoadImageUrl("https://graph.facebook.com/1466428176/picture?height=500", foto);
                utils.LoadImageUrl("https://graph.facebook.com/1466428176/picture?height=500", foto2);
                utils.LoadImageUrl("https://graph.facebook.com/1466428176/picture?height=500", Fullmage);
            } else {
                utils.LoadImage(fot, foto);
                utils.LoadImageUrl("https://graph.facebook.com/1466428176/picture?height=500", foto2);
                utils.LoadImage(fot, Fullmage);
            }

            usuario.setText(users);
            lugar.setText(places);
            info.setText(mensajes);

            AnimationBuilder modeBuilder = AnimationBuilder.newBuilder()
                    .addPart(1000, 60)
                    .addPart(1000, 60, 100)
                    .build();

            TextCounter.newBuilder()
                    .setTextView(comentarios)
                    .setType(TextCounter.LONG)
                    .setCustomAnimation(modeBuilder)
                    .from(0)
                    .to(cuenta)
                    .build()
                    .start();

            TextCounter.newBuilder()
                    .setTextView(likes)
                    .setType(TextCounter.LONG)
                    .setCustomAnimation(modeBuilder)
                    .from(0)
                    .to(cuenta2)
                    .build()
                    .start();
        }

    }

    private void CancelAutomatically(long milisegundos) {
        Handler handler = new Handler();
        handler.postDelayed(() -> finish(), milisegundos);
    }
}
