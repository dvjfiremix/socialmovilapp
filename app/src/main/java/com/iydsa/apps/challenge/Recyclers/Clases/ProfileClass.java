package com.iydsa.apps.challenge.Recyclers.Clases;

public class ProfileClass {
    private int foto;
    private String imageFoto;

    public ProfileClass() {

    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getImageFoto() {
        return imageFoto;
    }

    public void setImageFoto(String imageFoto) {
        this.imageFoto = imageFoto;
    }
}
