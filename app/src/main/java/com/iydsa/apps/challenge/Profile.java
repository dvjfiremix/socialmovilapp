package com.iydsa.apps.challenge;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.Recyclers.Adapters.ProfileAdapter;
import com.iydsa.apps.challenge.Recyclers.Clases.ProfileClass;

import java.util.ArrayList;

import Libraryes.Utils;
import de.hdodenhof.circleimageview.CircleImageView;

public class Profile extends AppCompatActivity {
    private static final String IG_OWNER = "https://www.instagram.com/Krizthian.a.r";
    CircleImageView foto, perfil;
    LinearLayout back;
    ProfileAdapter adapter;
    ArrayList<ProfileClass> profile = new ArrayList<>();
    ProfileClass profileClass;
    RecyclerView recycler;
    ImageView home;
    private Utils utils;
    TextView user;
    Button edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        utils = Utils.getInstance(this);
        foto = findViewById(R.id.perfil);
        back = findViewById(R.id.back);
        perfil = findViewById(R.id.foto);
        recycler = findViewById(R.id.homes);
        home = findViewById(R.id.home);
        user = findViewById(R.id.user);
        edit = findViewById(R.id.edit);
        edit.setText("Edit Profile");
        user.setText("@Krizthian.a.r");
        user.setOnClickListener(v -> startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(IG_OWNER))));
        edit.setOnClickListener(v -> utils.CustomMesaje("Editar perfil de este usuario", "https://graph.facebook.com/1466428176/picture?height=500"));
        home.setOnClickListener(v -> {
            Intent n = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(n);
            finish();
        });
        adapter = new ProfileAdapter(this, profile);
        recycler.setAdapter(adapter);
        utils.LoadImageUrl("https://graph.facebook.com/1466428176/picture?height=500", foto);
        utils.LoadImageUrl("https://graph.facebook.com/1466428176/picture?height=500", perfil);
        back.setOnClickListener(v -> {
            Intent n = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(n);
            finish();
        });
        LoadImages();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent n = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(n);
        finish();
    }

    private void LoadImages() {
        profileClass = new ProfileClass();
        profileClass.setImageFoto("https://scontent.fntr3-1.fna.fbcdn.net/v/t1.0-9/105608554_10217615494286954_3665641764364151479_n.jpg?_nc_cat=109&_nc_sid=09cbfe&_nc_eui2=AeGRjsyOsPGjsNhCIYsceoAxtJ4E3u07xOi0ngTe7TvE6D6yA34RitvF2S9-WX-yM-U&_nc_ohc=NO2tQyOo34MAX_GlQv0&_nc_ht=scontent.fntr3-1.fna&oh=474708880ae6c9ac51741e60348b897b&oe=5FA275A0");
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setImageFoto("https://scontent.fntr3-1.fna.fbcdn.net/v/t1.0-9/97997034_10217320509472518_2409226926477541376_o.jpg?_nc_cat=110&_nc_sid=174925&_nc_eui2=AeGQsnfIKVfGXYrANq8kH-zH4u42CVlXtsPi7jYJWVe2w03xGLaXTxQvoLUnDwTMUAY&_nc_ohc=TUx7lcGbAJEAX-ikccd&_nc_ht=scontent.fntr3-1.fna&oh=7c6d3f5f2e897e6c68e7135cbe84f4b9&oe=5FA450F1");
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setImageFoto("https://scontent.fntr3-1.fna.fbcdn.net/v/t1.0-9/80044632_10216047557169506_8133368315620884480_o.jpg?_nc_cat=110&_nc_sid=174925&_nc_eui2=AeHnzXY3OBiBdRPYnAShIGUTiGjqTVpbnbmIaOpNWludueNa70GSTJcNz5iiIOPYBzw&_nc_ohc=-YP-feQX_JIAX9S0w-H&_nc_ht=scontent.fntr3-1.fna&oh=33c01ff19cd9bd4f82a91121ec888518&oe=5FA13E19");
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setImageFoto("https://www.mo.agency/hubfs/So%20you%20want%20to%20be%20a%20web%20developer.png");
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setImageFoto("https://www.simplilearn.com/ice9/free_resources_article_thumb/tester-or-developer-what-suits-you-the-most.jpg");
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setImageFoto("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRu3qvNL4adsWiEEtB84ZAulr9W4I-Gd60ACQ&usqp=CAU");
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.dev1);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.dev2);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.carlos);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.carlos2);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.carlos3);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.carlos4);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.carlos5);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.carlos6);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.profile);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.image_10);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.image_14);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.image_17);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.image_15);
        profile.add(profileClass);

        profileClass = new ProfileClass();
        profileClass.setFoto(R.drawable.image_16);
        profile.add(profileClass);

        adapter.notifyDataSetChanged();
    }
}
