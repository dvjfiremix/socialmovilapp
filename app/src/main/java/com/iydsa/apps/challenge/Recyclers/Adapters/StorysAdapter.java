package com.iydsa.apps.challenge.Recyclers.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.FullScreen;
import com.iydsa.apps.challenge.R;
import com.iydsa.apps.challenge.Recyclers.Clases.storyclass;
import com.iydsa.apps.challenge.Recyclers.Holders.StoryHolder;

import java.util.ArrayList;

import Libraryes.Utils;

public class StorysAdapter extends RecyclerView.Adapter<StoryHolder> {
    private Context context;
    private ArrayList<storyclass> stories;
    private Utils utils;

    public StorysAdapter(Context ctx, ArrayList<storyclass> stories) {
        this.context = ctx;
        this.stories = stories;
        utils = Utils.getInstance(ctx);
    }

    @NonNull
    @Override
    public StoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.storie_design, null);
        return new StoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StoryHolder holder, int position) {
        final storyclass datos = stories.get(position);
        if (position == 0) {
            holder.mifoto.setVisibility(View.VISIBLE);
            holder.other.setVisibility(View.GONE);
            utils.LoadImageUrl(datos.getImageUrl(), holder.foto);
            holder.mifoto.setOnClickListener(v -> utils.CustomMesaje("Este es la opción para agregar historias de la red social", datos.getFoto()));
        } else {
            holder.other.setVisibility(View.VISIBLE);
            holder.mifoto.setVisibility(View.GONE);
            if (position == 1) {
                utils.LoadImageUrl(datos.getImageUrl(), holder.otra);
            } else {
                utils.LoadImage(datos.getFoto(), holder.otra);
            }

            holder.other.setOnClickListener(v -> {
                Intent n = new Intent(context, FullScreen.class);
                n.putExtra("FOTO", datos.getFoto());
                n.putExtra("FOTO2", datos.getFoto2());
                n.putExtra("USUARIO", datos.getUser());
                n.putExtra("PLACE", datos.getPlace());
                n.putExtra("COMENTARIOS", datos.getCuent_comment());
                n.putExtra("LIKES", datos.getCuenta_like());
                n.putExtra("INFO", datos.getTexto());
                context.startActivity(n);
            });
        }
    }

    @Override
    public int getItemCount() {
        return stories.size();
    }
}
