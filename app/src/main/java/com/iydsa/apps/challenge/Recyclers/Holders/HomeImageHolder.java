package com.iydsa.apps.challenge.Recyclers.Holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.R;

public class HomeImageHolder extends RecyclerView.ViewHolder {
    public ImageView Homeimage;
    public TextView mensajes;
    public TextView likes;

    public HomeImageHolder(View itemView) {
        super(itemView);
        Homeimage = itemView.findViewById(R.id.foto_home);
        mensajes = itemView.findViewById(R.id.comment);
        likes = itemView.findViewById(R.id.likes);

    }
}
